#!/bin/bash

#stop application
echo stop application
pm2 stop parcel-server

echo build:prod application
#build application
npm start:prod

echo build success

echo start application
#restart application with pm2
pm2 start parcel-server
echo deploy success


pm2 logs --no-daemon