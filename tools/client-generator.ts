import * as path from 'path';
import * as colors from 'colors/safe';
import { execSync } from 'child_process';

const apiSwaggerUrl = 'http://localhost:3000/api/docs/swagger.json';
const swaggerClientOutput = path.join(__dirname, '..', '..', 'parcels', 'lib', 'api');
try {
    execSync(`npx openapi-generator-cli generate -i ${apiSwaggerUrl} -g dart -o ${swaggerClientOutput}`).toString();
    console.log(colors.green(`Code client généré avec succès !`));

} catch (error) {
    console.log("🚀 >> error.status", error.status)
    console.log("🚀 >> error.message", error.message)
    console.log("🚀 >> error.stdout", error.stdout)
}
