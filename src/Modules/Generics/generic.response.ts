export class GenericResponse {
    message: string;
    success: boolean;
    handleError(e: string) {
        this.success = false;
        this.message = e;
    }
}