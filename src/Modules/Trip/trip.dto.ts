import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsString, IsInt, Length } from 'class-validator';
import { UserDto } from "../User/user.dto";
import { ParcelType } from "./trip.entity";

export class TripDto {
    @Length(36, 36)
    @IsString()
    id: string;

    @Length(36, 36)
    @IsString()
    userId: string;
    startPlace: string;
    arrivalPlace: string;
    parcelType: ParcelType;
    kilo?: number;
    startDate: Date;
    user?: UserDto;
    planeTicketPath?: string;
}