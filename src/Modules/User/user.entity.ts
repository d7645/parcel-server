import { Column, Entity, OneToMany } from "typeorm";
import { AppBaseEntity } from "../../Base/base.entity";
import { TripDto } from "../Trip/trip.dto";
import { Trip } from "../Trip/trip.entity";
import { UserRole } from "./roles/user-role.entity";
import { UserDto } from "./user.dto";

@Entity({ name: 'user' })
export class User extends AppBaseEntity {
    @Column('varchar', { name: 'email', nullable: false, length: 40, unique: true })
    email: string;

    @Column('varchar', { name: 'lastname', nullable: false, length: 40 })
    lastname: string;

    @Column('varchar', { name: 'firstname', nullable: false, length: 40 })
    firstname: string;

    @Column('varchar', { name: 'password', nullable: true, length: 100 })
    password: string;

    @Column('varchar', { name: 'salt' })
    salt?: string;

    @Column('varchar', { name: 'refreshToken', nullable: true })
    refreshToken?: string;

    @OneToMany(() => UserRole, userRole => userRole.user, { onDelete: 'SET NULL', nullable: true, cascade: true })
    userRoles?: UserRole[];

    @OneToMany(() => Trip, trip => trip.user, { onDelete: 'CASCADE', cascade: true })
    trips: Trip[];

    toDto(): UserDto {
        return {
            id: this.id,
            email: this.email,
            password: this.password,
            lastname: this.lastname,
            firstname: this.firstname,
            refreshToken: this.refreshToken,
            userRoles: this.userRoles ? this.userRoles.map(x => x.toDto()) : [],
            trips: this.trips ? this.trips.map(x => x.toDto()) : [],
        }
    }

    fromDto(dto: UserDto) {
        this.id = dto.id;
        this.email = dto.email;
        this.password = dto.password;
        this.lastname = dto.lastname;
        this.firstname = dto.firstname;
        this.refreshToken = dto.refreshToken;
        if (dto.userRoles) {
            this.userRoles = dto.userRoles.map<UserRole>(x => {
                const role = new UserRole();
                role.fromDto(x);
                return role;
            })
        }

        if (dto.trips) {
            this.trips = dto.trips.map<Trip>(x => {
                const trip = new Trip();
                trip.fromDto(x);
                return trip;
            })
        }
    }
}