import { Column, Entity, JoinColumn, ManyToOne } from "typeorm";
import { AppBaseEntity } from "../../../Base/base.entity";
import { UserRoleDto } from "./user-role.dto";
import { User } from "../user.entity";
import { Role } from "./role.entity";

export enum RoleName {
    ADMIN = "admin",
    USER = "user",
}

@Entity({ name: 'user-role' })
export class UserRole extends AppBaseEntity {

    @Column('varchar', { name: 'userId', length: 36, nullable: true })
    userId?: string;

    @ManyToOne(() => User, user => user.userRoles)
    @JoinColumn({ name: 'userId' })
    user?: User;

    @Column('varchar', { name: 'roleId', length: 36, nullable: true })
    roleId: string;

    @ManyToOne(() => Role)
    @JoinColumn({ name: 'roleId' })
    role?: Role;

    toDto(): UserRoleDto {
        return {
            id: this.id,
            userId: this.userId,
            roleId: this.roleId,
            role: this.role ? this.role.toDto() : null,
        }
    }

    fromDto(dto: UserRoleDto) {
        this.id = dto.id;
        this.userId = dto.userId;
        if (dto.role)
            this.role.fromDto(dto.role);
    }
}