import { ApiPropertyOptional } from "@nestjs/swagger";
import { UserRoleDto } from "./user-role.dto";

export class RoleDto {
    @ApiPropertyOptional()
    id: string;

    @ApiPropertyOptional()
    label: string;

    @ApiPropertyOptional()
    code: string;
}