import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Environment } from "../../../environment/environment";
import { JwtStrategy } from "./strategy/passport.strategy";
import { UserRole } from "./roles/user-role.entity";
import { UserController } from "./user.controller";
import { User } from "./user.entity";
import { UserService } from "./user.service";
import { Role } from "./roles/role.entity";

@Module({
    controllers: [
        UserController
    ],
    imports: [
        TypeOrmModule.forFeature([
            User,
            UserRole,
            Role,
        ]),
        PassportModule.register({
            defaultStrategy: 'jwt',
        }),
        JwtModule.register({
            secret: Environment.jwt_secret,
            signOptions: {
                expiresIn: 3600
            }
        })
    ],
    providers: [
        UserService, JwtStrategy
    ]
})
export class UserModule { }