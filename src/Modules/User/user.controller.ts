import { Body, Controller, Get, HttpCode, Param, Post, Req, UseGuards } from "@nestjs/common";
import { ApiBearerAuth, ApiOperation, ApiResponse } from "@nestjs/swagger";
import { Request } from "express";
import { Roles } from "../../Base/decorator/role.decorator";
import { User } from "../../Base/decorator/user.decorator";
import { JwtAuthGuard } from "./Guards/jwt-auth.guard";
import { RolesGuard } from "./Guards/roles-guard";
import { RoleName } from "./roles/user-role.entity";
import { GetUserResponse, GetUsersResponse, LoginModel, LoginResponse, SubscribeRequest, SubscribeResponse, UserDto } from "./user.dto";
import { UserService } from "./user.service";

@Controller('user')
export class UserController {
    constructor(
        private userService: UserService
    ) {

    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(RoleName.USER)
    @Get()
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Get all users', operationId: 'getUsers' })
    @ApiResponse({ status: 200, description: 'Get all users', type: GetUsersResponse })
    @HttpCode(200)
    getUsers() {
        return this.userService.getUsers();
    }

    @Get('/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Get one user', operationId: 'getUser' })
    @ApiResponse({ status: 200, description: 'Get one users', type: GetUserResponse })
    @HttpCode(200)
    getUser(@Param('id') id: string) {
        return this.userService.getUser(id);
    }

    @ApiBearerAuth()
    @ApiOperation({ summary: 'create or update user', operationId: 'createOrUpdate' })
    @ApiResponse({ status: 200, description: 'Create or update user', type: GetUserResponse })
    @HttpCode(200)
    @Post()
    createOrUpdate(@Body() user: UserDto) {
        console.log("🚀 ~ createOrUpdate ~ user", user)

    }

    @Post('subscribe')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'subscribe user', operationId: 'subscribeUser' })
    @ApiResponse({ status: 200, description: 'subscribe user', type: SubscribeResponse })
    @HttpCode(200)
    async subscribeUser(@Body() subscribeRequest: SubscribeRequest): Promise<SubscribeResponse> {
        return await this.userService.subscribe(subscribeRequest);
    }

    @Post('login')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'login user', operationId: 'loginUser' })
    @ApiResponse({ status: 200, description: 'login user', type: LoginResponse })
    @HttpCode(200)
    async loginUser(@Body() request: LoginModel): Promise<LoginResponse> {
        return await this.userService.login(request);
    }

    @Post('refresh-token')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'refresh token', operationId: 'refreshToken' })
    @ApiResponse({ status: 200, description: 'refresh token', type: LoginResponse })
    @HttpCode(200)
    async refreshToken(@Req() request: Request): Promise<LoginResponse> {
        return await this.userService.refreshToken(request);
    }
}