import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Request } from "express";
import { UserDto } from "../user.dto";
import { UserService } from "../user.service";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector,
        //  private userService: UserService,
    ) { }
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const roles = this.reflector.get<string[]>('roles', context.getHandler());
        if (!roles)
            return true;

        const request: Request = context.switchToHttp().getRequest();
        console.log("🚀 ~ canActivate ~ request", request.authInfo)
        const user = request.user as UserDto;

        if (!user.userRoles?.length) {
            return false;
        }

        return roles.some(x => user.userRoles.some(y => y.role.code === x));
    }
}