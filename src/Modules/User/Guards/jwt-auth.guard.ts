import { ExecutionContext, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard } from '@nestjs/passport';
import { UserService } from '../user.service';
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
}
