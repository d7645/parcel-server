import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { GenericResponse } from '../Generics/generic.response';
import { TripDto } from '../Trip/trip.dto';
import { UserRoleDto } from './roles/user-role.dto';


export class UserDto {
    @IsString()
    @Length(36, 36)
    id?: string;

    @IsEmail()
    @ApiPropertyOptional()
    email: string;

    @ApiPropertyOptional()
    password: string;

    @ApiPropertyOptional()
    lastname?: string;

    @ApiPropertyOptional()
    firstname?: string;

    @ApiPropertyOptional()
    refreshToken?: string;

    @ApiPropertyOptional({ type: () => UserRoleDto, isArray: true })
    userRoles?: UserRoleDto[];

    @ApiPropertyOptional({ type: () => TripDto, isArray: true })
    trips?: TripDto[];
}

export class LoginModel {
    @IsNotEmpty()
    @IsEmail()
    @ApiPropertyOptional()
    email: string;

    @IsNotEmpty()
    @ApiPropertyOptional()
    password: string;
}

export class SubscribeRequest {
    @ApiPropertyOptional({ type: () => UserDto })
    user: UserDto;
}

export class GetUserResponse extends GenericResponse {
    @ApiPropertyOptional({ type: () => UserDto })
    user: UserDto;
}

export class GetUsersResponse extends GenericResponse {
    @ApiPropertyOptional({ type: () => UserDto, isArray: true })
    users: UserDto[];
}


export class LoginResponse extends GenericResponse {
    @ApiPropertyOptional()
    accessToken?: string;
}

export class SubscribeResponse extends LoginResponse {
}