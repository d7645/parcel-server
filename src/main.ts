import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import helmet from 'helmet';
import { TestInterceptor } from './interceptors/test.interceptor';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('Parcel-server')
    .setDescription('Parcel server api')
    .setVersion('1.0')
    .addTag('parcel')
    .build();

  const document = SwaggerModule.createDocument(app, config);

  app.use('/api/docs/swagger.json', (req: any, res: any) => {
    res.send(document);
  });
  SwaggerModule.setup('api', app, document, {
    swaggerUrl: `/api/docs/swagger.json`,
    explorer: true,
    swaggerOptions: {
      docExpansion: 'list',
      filter: true,
      showRequestDuration: true,
    },
  });

  app.use(helmet());
  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  app.useGlobalInterceptors(new TestInterceptor());
  await app.listen(3000);
}
bootstrap();

//openapi-generator-cli generate -i "${apiSwaggerUrl}" -g typescript-angular -c "${swaggerCodegenConfig}" -o "${swaggerFrontOutput}" --type-mappings DateTime=Date
