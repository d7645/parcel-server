import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { RoleDto } from "../../Modules/User/roles/role.dto";
import { Role } from "../../Modules/User/roles/role.entity";
import { UserRoleDto } from "../../Modules/User/roles/user-role.dto";
import { RoleName, UserRole } from "../../Modules/User/roles/user-role.entity";

@Injectable()
export class DatabaseService {
    constructor(
        @InjectRepository(Role)
        private roleRepository: Repository<Role>,
    ) {
    }

    public async createInitial() {
        await this.createInitialRoles(RoleName.ADMIN, RoleName.USER);
    }

    private async createInitialRoles(...roles: string[]) {
        for (const role of roles) {
            const roleResponse = await this.roleRepository.findOne({ code: role });
            if (!roleResponse) {
                console.log(`creation role ${role}`);
                const roleDto = new RoleDto();
                roleDto.code = role;
                roleDto.label = role;

                await this.roleRepository.save(roleDto);
            }
        }
    }
}