import { PrimaryGeneratedColumn } from "typeorm";

export class AppBaseEntity {
    @PrimaryGeneratedColumn('uuid', { name: 'id' })
    id: string;
}