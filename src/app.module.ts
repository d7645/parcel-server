import { MorganMiddleware } from '@nest-middlewares/morgan';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Environment } from '../environment/environment';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './Base/services/database.module';
import { DatabaseService } from './Base/services/database.service';
import { FirstMiddleware } from './middlewares/first.middleware';
import { UserModule } from './Modules/User/user.module';

@Module({
  imports: [
    UserModule,
    DatabaseModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: Environment.db_host,
      port: Environment.db_port,
      username: Environment.db_user,
      password: Environment.db_password,
      database: Environment.db_name,
      entities: [
        __dirname + '/**/*.entity{.ts,.js}'
      ],
      synchronize: true,
    })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {

  constructor(
    private databaseService: DatabaseService,
  ) {
    this.databaseService.createInitial();
  }
  configure(consumer: MiddlewareConsumer) {
    MorganMiddleware.configure('dev');
    consumer.apply(MorganMiddleware, FirstMiddleware).forRoutes('');
  }

}
