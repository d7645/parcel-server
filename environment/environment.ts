import * as EnvData from "./env.json";
class AppEnvironment {
    app_port: number;
    db_type: string;
    db_port: number;
    db_user: string;
    db_password: string;
    db_host: string;
    db_name: string;
    jwt_secret: string;
    constructor() {
    }
}

const appEnvironment: AppEnvironment = new AppEnvironment();

for (const envKey in EnvData) {
    appEnvironment[envKey] = EnvData[envKey];
}

export const Environment = appEnvironment;

